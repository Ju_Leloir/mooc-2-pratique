[TOC]

## Comment poser une question qui n'est pas ici ?

Il suffit de nous contacter [via le formulaire du portail NSI](https://mooc-nsi-snt.gitlab.io/portail/7_Contact.html) ou via le [forum](https://mooc-forums.inria.fr/moocnsi/).

## Sommaire des questions abordées

### Comment s'inscrire admistrativement au CAPES ?

Les informations sont sur ce [lien général pour tous les CAPES](https://www.devenirenseignant.gouv.fr/pid35072/inscrivez-vous-aux-concours-recrutement-education.html) tandis que la [présentation institutionnelle du concours est ici](https://www.devenirenseignant.gouv.fr/pid33985/enseigner-college-lycee-general-capes.html).

### Qu'en est-il de l'agrégation ?

La présente formation peut aider, avec le travail personnel idoine, à se prépaper au CAPES. La préparation à l'agrégation est d'une autre ampleur.

La première agrégation externe d'informatique s'ouvre en 2022. Les inscriptions étaient (nécessaires et) ouvertes sur https://www.devenirenseignant.gouv.fr/ jusqu'au 17 novembre 2021 - 17h. _Nous communiquerons pour la prochaine session de 2023_

De plus, le jury a ouvert un site lui permettant de diffuser des éléments d'information. Ce site n'a pas de caractère officiel mais permet une communication plus rapide que le site devenirenseignant.gouv.fr, qui seul fait foi
    https://agreg-info.org/
On y retrouvere en particulier :
- la liste des leçons,
- les sujets 0 des épreuves 1 et 2,
- une foire aux questions, et un moyen de proposer des questions pour inclusion dans la FAQ.

Une liste de diffusion est également crée pour les futures informations concernant cette agrégation   https://groupes.renater.fr/sympa/info/agreg-info, cette liste n'a pas vocation à être un lieu d'échange, mais un outil de communication type "newsletter".

Par rapport au programme du CAPES, le [programme de l'agrégation](https://media.devenirenseignant.gouv.fr/file/agregation_externe_21/22/3/p2022_agreg_ext_informatique_1414223.pdf) va plus loin, comme détaillé sur ce lien.

Par exemple :

- La partie calculabilité, nous ne sommes la découverte des notions, mais dans un vrai cours de calculabilité, qui inclut les modèle de calcul (machines de Turing : définition, principales variantes (ruban biinfini vs infini, machine à plusieurs rubans)
  - La machine de Turing est le modèle de calcul retenu pour l’étude des notions de calculabilité, universalité, décidabilité, indécidabilité,problème de l’arrêt. 
  − Complexité : complexité en temps et en espace, classe P, acceptation par certificat, classe NP, réduction polynomiale.,NP-complétude, théorème de Cook. 
- La théorie des langages et compilation est développée
  - Chaîne de compilation, analyse lexicale, analyse syntaxique (principes de l’analyse descendante)
  - Analyse sémantique élémentaire (arbre de syntaxe abstraite, environnement, analyse deportée, typage)
  - Génération de code vers une machine à pile.


