## Données en tables

### Fiche 1 : Découvrir les formats CSV et TSV

#### Contexte et pré-requis

Vous avez une séance de TP (devant machines). Les élèves ont déjà rencontré la notion de fichiers textes et les formats CSV et TSV ont été présentés dans une séance de cours. Mais ils n'ont encore jamais manipulé : ni les fichiers avec Python, ni bien sûr les formats en question.

#### Travail demandé

Proposer une suite d'activités _découvertes_ pour une séance de 2h. La séance se termine par un mini QCM de 10 questions.

- Utilisez-vous le tableur ? 
- Comment mettez-vous en avant l'utilité d'un traitement par langage de programmation ?
- Comment abordez-vous le problème des séparateurs ? 
- Illustrez-vous les deux types de tables, avec et sans descripteurs ?
- Pour chacune des questions de votre QCM, dites ce que la question est censée mesurer
