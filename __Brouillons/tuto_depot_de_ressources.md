# Déposer une ressource pour travailler entre pair·e·s

Dans cette formation pour apprendre à enseigner l'informatique, nous créons des fiches et ressources, pour des activités et leur rendus. Comment les partager (entre nous, ou plus largement avec d'autres collègues) ?

On se propose de le faire sur la plateforme de dépôt [GITLAB](https://fr.wikipedia.org/wiki/GitLab) basée sur [GIT](https://fr.wikipedia.org/wiki/Git). Voici comment ?

### Rejoindre l'espace de partage

- S'[incrire](https://gitlab.com/users/sign_up) ou se [connecter](https://gitlab.com/users/sign_in) si vous avez déjà un compte
- Aller sur la [page du projet](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources) et y [demander l'accès](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources/-/project_members/request_access) comme [illustré ici](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/Ressources/Images/request-access.png)
  - Nous [validerons l'accès](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources/-/project_members?tab=access_requests) en tant que co-développeur qq heures plus tard.

    NOTE IL FAUDRA CLIQUER POUR CHAQUE PERSONNE !

### Y déposer une ressources 

- Bien prendre connaissance de [l'arborescence](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources) 
- Suivre une règle de _nommage précise_ : `nom_du_fichier_$login-sur-gitlab.md`
  - par exemple `fiche_module1_shorau.md` où `shorau` est votre login
- Déposer la fiche en allant dans le bon répertoire par exemple `https://gitlab.com/mooc-nsi-snt/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/` en utilisant la fonction de _upload_,  comme [illustré ici](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/Ressources/Images/upload-file.png)
- Bien noter le lien du fichier lui même par exemple `https://gitlab.com/mooc-nsi-snt/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_module1_shorau.md` avec cet exemple

### Aller sur le MOOC FUN déposer le lien de cette ressource 

A COMPLETER
