## Questions interview

1. Raconte-nous un peu ton parcours, comment es-tu devenu prof de NSI et de SNT ?

2. As-tu modifié tes pratiques pédagogiques par rapport à ce que tu faisais dans ta discipline d’origine ?

3. (si la réponse à la question ci-dessus est oui) : Toujours à propos de tes pratiques pédagogiques, peux-tu nous donner quelques exemples qui te semblent marquants

4. Quelle est la principale difficulté que tu rencontres aujourd’hui dans ta pratique quotidienne.

5. (question optionnelle) Aurais-tu un conseil à donner aux futurs enseignants de SNT et NSI ?

