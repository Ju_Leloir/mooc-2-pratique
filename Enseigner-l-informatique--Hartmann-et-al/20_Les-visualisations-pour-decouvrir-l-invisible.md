---
title: 20. Les visualisations pour découvrir l'invisible
---

# Les visualisations pour découvrir l'invisible

K., enseignante en informatique, a profité de la semaine de rentrée pour
faire démonter des ordinateurs usagés. Les étudiantes ont
particulièrement apprécié cette activité qui leur a permis d'avoir une
vision concrète de l'intérieur d'un ordinateur, et ont ainsi pu
découvrir l'aspect réel des principaux composants (processeur, mémoire,
disque dur, lecteur de DVD, ventilateur) qu'elles ne connaissaient
jusqu'alors que sous la forme des cadres sur les représentations
schématiques. Cependant, l'enseignement de l'informatique n'offre
malheureusement que rarement la possibilité de démonter et d'observer
quelque chose physiquement. Lorsque l'enseignant K. présente les
principaux protocoles Internet, elle ne peut pas découper un câble
réseau et faire observer les signaux électriques. Même si ces derniers
étaient visibles, ils ressembleraient à une suite de bits telle que:
0110011011010111000111010000011101010100.

---

**Problème :** l'informatique est le domaine de l'invisible. Très peu
d'éléments peuvent être observés et examinés directement sans accessoire
approprié.


Il existe deux raisons essentielles pour lesquelles peu d'observations
directes sont possibles en informatique. La première est que les
ordinateurs manipulent des symboles. Le niveau le plus bas du monde
numérique contient en tout et pour tout deux symboles, à savoir les
états binaires 0 et 1 représentés par un bit. Il n'est pas possible de
« voir » les bits envoyés sur la connexion Internet d'un ordinateur, de
percevoir directement l'importance de ce flux de 0 et de 1 et encore
moins de le manipuler de manière appropriée. Les informations mémorisées
dans les bits ne deviennent compréhensibles aux humains qu'après avoir
été réunies en unités sémantiques plus grandes. De plus, les ordinateurs
se caractérisent par des phénomènes dynamiques. La bonne compréhension
de ces phénomènes impose de tenir compte du temps d'exécution, qui ne
peut cependant pas être observé directement. En outre, les phénomènes se
déroulent généralement trop vite pour l'observateur humain.


Les symboles invisibles et les processus dynamiques peuvent être rendus
visibles à l'aide de ce que l'on appelle des visualisations. Ces
visualisations se rencontrent constamment en informatique, mais aussi
dans d'autres disciplines ou dans la vie quotidienne. La représentation
schématique des différentes couches de la pile de protocole TCP/IP ou
encore un diagramme Entité-Relation sont également des visualisations.

Nous allons nous concentrer ci-après sur deux techniques de
visualisation concrètes : la première est la simple matérialisation de
l'état actuel d'un composant d'ordinateur en regroupant les 0 et les 1
en unités d'information plus grandes, comme évoqué ci-dessus. Une autre
technique est la simulation. Les processus sont ici reproduits dans un
modèle qui permet de mener des expériences.

Quelle que soit la technique choisie, il existe trois sources d'outils
de visualisation :

**utilisation d'outils standard**  
Les outils standard contiennent
souvent des fonctions de visualisation très utiles qui peuvent être
utilisées dans l'enseignement, par exemple le Gestionnaire de tâches de
Windows ou l'affichage du texte source d'une page Web dans le
navigateur.

**recherche de ce qui existe**  
Des visualisations sur un sujet précis
sont souvent proposées gratuitement sur Internet. Pour les trouver, une
recherche avec un mot-clé comme « applet » ou « simulation » combiné
avec quelques termes en rapport avec le contenu est souvent suffisante.

En alternative aux moteurs de recherche traditionnels, il existe des
portails spécifiquement dédiés à l'enseignement comme, par exemple,
celui du SIGCSE, le Special Interest Group on Computer Science Education
de l'Association for Computing Machinery (ACM) : le lien SIGCSE
Education Links contient une catégorie propre Visualizations
(http://www.sigcse.org/resources/links).

**développement propre**  
Il faut ici être très prudent. Du fait de
l'effort généralement important, les développements propres ne se
justifient qu'à condition d'être utilisés pendant une longue période et
par plusieurs étudiants.

**Solution :** les informations et les processus invisibles à
l'intérieur d'un ordinateur peuvent être rendus visibles par des moyens
appropriés. Des outils de visualisation permettent d'observer
concrètement certaines situations et d'examiner plus précisément le
temps d'exécution d'une opération à l'aide d'une simulation.

**Exemple 1 : observation des réseaux**

L'enseignant K. utilise un outil appelé Ethereal (www.ethereal.com) pour
visualiser le trafic du réseau. Ethereal peut « écouter » les données
qui entrent et qui sortent par la connexion réseau d'un ordinateur et
les représenter dans un tableau. Cet outil connaît de nombreux
protocoles dont il peut interpréter et afficher les données (Fig. 20.1).
Les étudiants disposent ainsi d'un accessoire leur permettant d'observer
et d'analyser dans un environnement réel ce qu'ils ont appris en
théorie. Ils n'ont pas besoin de s'occuper de la syntaxe précise des
différents paquets de données et, au lieu de cela, ils peuvent se
concentrer sur le comportement d'exécution, c'est-à-dire les messages
échangés entre l'émetteur et le récepteur.

<img alt="Fig. 20.1" src="Images/20.1_figure.jpg" width="450px"/>

**Fig. 20.1.** Ethereal affiche dans la partie supérieure la
communication entre un navigateur et un serveur Web et dans la partie
inférieure les détails de l'interrogation HTTP.

Un autre avantage lié à l'utilisation d'Ethereal est que l'enseignant
peut générer et enregistrer le trafic réseau à étudier au calme avant le
cours. Au moment de la classe, elle distribue alors un fichier contenant
les enregistrements et les étudiants reçoivent tous une situation
initiale parfaitement définie pour leurs analyses.

**Exemple 2 : simulation d'un protocole Peer-to-Peer**

L'enseignant M. utilise un simulateur pour l'introduction au protocole
Peer-to-Peer. Les étudiants peuvent ainsi mettre en place différentes
topologies de réseau, simuler le protocole dans les conditions générales
correspondantes puis observer en détail et étudier l'échange de paquets
de protocole.

M. utilise le simulateur pour que les étudiants, en petits groupes,
puissent comprendre, vérifier et répéter le savoir transmis pendant le
cours théorique. Le simulateur peut également être utilisé pour
l'apprentissage par la découverte ; dans ce cas, les étudiants reçoivent
pour l'essentiel le simulateur ainsi qu'une courte notice et une
introduction. Ils commencent ensuite par apprendre à connaître le
protocole et mènent alors leurs recherches en fonction de leurs propres
découvertes, par exemple dans les domaines de la sécurité, de
l'anonymat, de l'efficacité et des optimisations possibles.

**Exemple 3 : utilisation d'outils standard pour les visualisations**

Le Gestionnaire de périphériques de Windows peut être utilisé comme un
outil de visualisation simple (Fig. 20.2) dans le cadre d'une
description et d'une explication des différents composants d'un
ordinateur. Il s'avère ici particulièrement utile, car il montre
également les composants qui ne sont pas visibles depuis l'extérieur,
certains pas même après avoir ouvert l'ordinateur. Ce mode de
visualisation reste bien évidemment rudimentaire, il n'indique par
exemple aucune relation entre les composants.

De nombreux systèmes d'exploitation possèdent également des outils -- ou
utilitaires --permettant également d'afficher la charge actuelle de
l'ordinateur. Ceux-ci sont présents en standard sous Linux et autres OS
similaires à Unix, l'un des plus importants a pour nom vmstat. Il peut
afficher différentes caractéristiques de charge de la machine avec un
rafraîchissement toutes les secondes.

La Figure 20.3 donne un exemple : pendant les quelques premières
secondes, il n'y a aucune charge du système et la colonne cpu id indique
100, ce qui correspond à « 100 % d'inactivité » ou de disponibilité. Un
gros fichier est ensuite copié d'un endroit à un autre sur le disque
dur, ce qui provoque une légère charge de l'ordinateur ainsi que la
lecture/écriture de blocs de données depuis/sur le disque dur (colonnes
*io bi* et *io bo*).

Cette visualisation peut servir à l'enseignant pour aborder différentes
autres valeurs et ainsi le comportement du système. Il peut par exemple
souligner le fait que la première copie a été suivie, peu de temps
après, d'une deuxième copie du même fichier, ce qui est reconnaissable à
la valeur 33439 dans la colonne *io bo*. La question qui se pose alors
est pourquoi n'existe-t-il pas de valeur correspondante dans la colonne
*io bi*, c'est-à-dire comment se fait-il que des données soient écrites
sur le disque dur alors qu'aucune donnée n'en est lue ?


<img alt="Fig. 20.2" src="Images/20.2_figure.jpg" width="450px"/>

**Fig. 20.2.** Gestionnaire de périphériques Windows



<img alt="Fig. 20.3" src="Images/20.3_figure.jpg" width="450px"/>

**Fig. 20.3.** Évolutions de quelques caractéristiques de la charge de
l'ordinateur sous Linux



**Exemple 4 : algorithmes de tri**

Les algorithmes de tri représentent un domaine classique des
visualisations. Ces dernières permettent ici de mieux comprendre les
processus dynamiques qui se déroulent pendant l'exécution de
l'algorithme. La Figure 20.4 représente une étape de traitement de
l'algorithme de tri Selection Sort.

<img alt="Fig. 20.4" src="Images/20.4_figure.jpg" width="450px"/>

**Fig. 20.4.** Visualisation de différents algorithmes de tri

L'applet illustré provient du projet Animal, un outil d'animation qui
met l'accent sur la visualisation des algorithmes. Un référentiel
contenant un grand nombre d'animations se trouve sur le site Web
www.animal.ahrgr.de.
