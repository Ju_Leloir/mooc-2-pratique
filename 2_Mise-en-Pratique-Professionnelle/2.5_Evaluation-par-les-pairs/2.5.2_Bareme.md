# Grille d'évaluation

## Penser, Concevoir, Élaborer (10 points possibles)

Il s'agit ici de mesurer l'adéquation de quelques unes des informations importantes de la fiche _prof_ au regard de l'activité élève proposée.

1. Existence de la fiche _prof_
    - Aucun point si la fiche est absente
    - 1 point si la fiche est présente avec les points suivants : la thématique, les notions liées, le résumé et les objectifs
    - 2 points si en plus on note la présence de : l'auteur, la durée, la forme de participation et le matériel nécessaire
2. La thématique
    - Aucun point : si la thématique n'a rien à voir avec l'activité (probablement une erreur de lien)
    - 1 point : la thématique ne semble pas tout à fait correspondre à l'activité ou s'il s'agit d'un hors programme
    - 2 points : tout va bien
3. Les notions liées
    - Aucun point : aucun rapport entre l'activité proposée et les notions citées
    - 1 point : certaines notions citées n'ont pas réellement rapport avec l'activité
    - 2 points : tout va bien
4. Le résumé
    - 1 point : le résumé manque un peu de précision
    - 2 points : tout va bien
5.  Les objectifs
    - 1 point : les objectifs manquent un peu de précision ou alors l'activité élève ne semble pas vraiment permettre d'atteindre les objectifs visés
    - 2 points : tout va bien

## Mettre en oeuvre, Animer (8 points possibles)

- 2 points si la fiche d'analyse de l'activité existe mais se limite à une sorte de résumé de l'activité
- 4 points : la fiche décrit sommairement le déroulement de l'activité en classe, mais il n'est pas précisé comment travaillent les élèves (indiviuellement ? en groupe ?)
- 6 points : bonne fiche d'analyse, toutefois le _timing_ est absent ou alors semble difficilement réalisable
- 8 points : excellente fiche d'analyse, complète et réaliste.

## Accompagner (8 points possibles)

Il s'agit d'évaluer que l'activité complémentaire proposée (remédiation, complément ou mini projet) est en adéquation avec l'activité initiale.

Comptez :

- 2 points si l'activité existe mais ne correspond pas du tout (trop difficile, hors sujet...)
- 4 points pour une activité qui correspond partiellement
- 6 points pour une bonne activité (il pourra y avoir quelques améliorations possibles)
- 8 points pour une très bonne activité, jouant par exemple sur la diversité par rapport à l'activité originale.

## Évaluer (8 points possibles)

Utiliser la partie de la grille qui correspond au travail que vous évaluez :

### Mini projet

Comptez :

- 2 points pour une grille d'évaluation du projet présente mais bien trop imprécise pour aider à évaluer les projets élèves
- 4 points si l'évaluation présente des déséquilibres
- 6 points pour une bonne grille d'évaluation pouvant être améliorée (par exemple la partie orale n'est pas précisée)
- 8 points pour une très bonne grille d'évaluation, précise et détaillée

### Devoir sur table

Comptez :

- 2 points pour un devoir sur table existant mais irréaliste : beaucoup trop long ou beaucoup trop difficile, voir à la limite du hors sujet par rapport à l'activité originale
- 4 points pour un devoir améliorable : à ajuster sur la difficulté ou la longueur
- 6 points pour un bon devoir sur table mais avec un point de détail à améliorer (comme par exemple un barème inexistant ou légèrement déséquilibré)
- 8 points pour un bon devoir, longueur et difficulté adaptées, barème précis
