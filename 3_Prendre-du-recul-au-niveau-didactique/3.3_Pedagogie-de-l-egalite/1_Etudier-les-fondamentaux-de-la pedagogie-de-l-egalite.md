## Appliquer une pédagogie de l’égalité dans les enseignements d’informatique

Comme premier travail nous vous proposons la lecture approfondie de cet <a href="https://interstices.info/appliquer-une-pedagogie-de-legalite-dans-les-enseignements-dinformatique/" target="_blank">article scientifique sur la pédagogie de l'égalité</a> qui s'articule autour de cette toile:

![Toile de la pédagogie de l'égalité](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/3_Prendre-du-recul-au-niveau-didactique/3.3_Pedagogie-de-l-egalite/Ressources/toile-egal.png)

Cette toile prend en considération la totalité de l’activité enseignante, et propose de revisiter sa pratique en vue d’une pédagogie de l’égalité en listant un certain nombre de points de vigilance.


Après le constat que nous connaissons toutes et tous de la situation très négative vis à vis de la participation des lycéennes à l'enseignement scientifique (dont l'informatique) et en ingénérie, cet article s'attaque tout de suite au problème en questionnant les idées usuellement admises sur le sujet, et s'appuie sur cette remise en cause pour proposer un vrai cadre théorique qui propose de travailler à construire directement un environnement pédagogique égalitaire permettant d’établir pour tous et toutes un rapport aux savoirs aussi indépendant que possible des rapports sociaux.

Le travail est issu du travail de recherche (Collet, 2018) : « <a href="https://archive-ouverte.unige.ch/unige:109428" target="_blank">Dépasser les éducations à : vers une pédagogie de l’égalité en formation initiale du personnel enseignant</a> ». Recherches Féministes, 31(1), 179-197. La version présentée ici est sa dernière évolution.
